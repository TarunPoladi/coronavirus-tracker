# Coronavirus Tracker API and Dashboard

[![pipeline status](https://gitlab.com/monitaur.ai/coronavirus-tracker/badges/master/pipeline.svg)](https://gitlab.com/monitaur.ai/coronavirus-tracker/commits/master)

Tracking the development of the Novel Coronavirus COVID-19.

- Dashboard: https://covid.monitaur.ai
- API: https://covid.monitaur.ai/api/coronavirus/summary

## Getting Started

```sh
$ docker-compose up -d --build
$ docker-compose exec web python manage.py create_db
```

Sanity check:

1. [http://localhost:5003/](http://localhost:5003/)
1. [http://localhost:5003/api/ping](http://localhost:5003/api/ping)
1. [http://localhost:5003/doc/](http://localhost:5003/doc/)
1. [http://localhost:5003/api/coronavirus/summary](http://localhost:5003/api/coronavirus/summary)
1. [http://localhost:5003/admin/](http://localhost:5003/admin/)

## Test

Run the tests:

```sh
$ docker-compose exec web python -m pytest "project/tests" -p no:warnings --cov="project"
```

Lint:

```sh
$ docker-compose exec web flake8 project
```

Run Black and isort:

```sh
$ docker-compose exec web black project
$ docker-compose exec web /bin/sh -c "isort project/**/*.py"
```

## Data

Data sourced from [2019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository by Johns Hopkins CSSE](https://github.com/CSSEGISandData/COVID-19). It's transformed with Pandas and then cached for 2 hours.

Curious about the prediction? Review [project/api/models.py](https://gitlab.com/monitaur.ai/coronavirus-tracker/-/blob/master/project/api/models.py).
