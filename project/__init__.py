# project/__init__.py


import os

from flask import Flask, request, redirect
from flask_admin import Admin
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from werkzeug.contrib.fixers import ProxyFix


# instantiate the extensions
db = SQLAlchemy()
cors = CORS()
admin = Admin(template_mode="bootstrap3")


def create_app(script_info=None):

    # instantiate the app
    app = Flask(__name__)
    app = Flask(
        __name__, template_folder="./client/templates", static_folder="./client/static",
    )
    app.wsgi_app = ProxyFix(app.wsgi_app)

    # set config
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)

    # set up extensions
    db.init_app(app)
    cors.init_app(app, resources={r"*": {"origins": "*"}})
    if os.getenv("FLASK_ENV") == "development":
        admin.init_app(app)

    # register blueprints
    from project.client.views import core_blueprint

    app.register_blueprint(core_blueprint)

    # register api
    from project.api import api

    api.init_app(app)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    @app.before_request
    def force_https():
        if (
            app.env != "development"
            and request.endpoint in app.view_functions
            and request.headers.get("X-Forwarded-Proto") == "http"
        ):
            url = request.url.replace("http://", "https://")
            code = 301
            return redirect(url, code=code)

    return app
