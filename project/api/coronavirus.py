# project/api/coronavirus.py

import json

from flask_restx import Namespace, Resource

from project.api.utils import calculate_totals, get_data, get_states

coronavirus_namespace = Namespace("coronavirus")


class Coronavirus(Resource):
    def get(self):
        confirmed_data_raw, confirmed_data_json, last_updated, regions = get_data()
        payload = {
            "confirmed_cases": json.loads(confirmed_data_json),
            "last_updated": last_updated,
        }
        return payload


class CoronavirusSummary(Resource):
    def get(self):
        confirmed_data_raw, confirmed_data_json, last_updated, regions = get_data()
        states = get_states(regions)
        confirmed_cases = json.loads(confirmed_data_json)

        payload = {
            "summary": calculate_totals(confirmed_cases, states),
            "last_updated": last_updated,
            "states": states,
        }
        return payload


coronavirus_namespace.add_resource(Coronavirus, "/")
coronavirus_namespace.add_resource(CoronavirusSummary, "/summary")
