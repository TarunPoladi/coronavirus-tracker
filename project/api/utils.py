# project/api/utils.py
import os
from datetime import datetime

import pandas as pd
from cachetools import TTLCache, cached
from flask import current_app
from monitaur import Monitaur

from project.api.models import arma_model, kalman_filter, kalman_filter_predict

BASE_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Confirmed.csv"  # noqa: E501
STATES = [
    "Alaska",
    "Arizona",
    "Arkansas",
    "California",
    "Colorado",
    "Connecticut",
    "District of Columbia",
    "Delaware",
    "Florida",
    "Georgia",
    "Hawaii",
    "Idaho",
    "Illinois",
    "Indiana",
    "Iowa",
    "Kansas",
    "Kentucky",
    "Louisiana",
    "Maine",
    "Maryland",
    "Massachusetts",
    "Michigan",
    "Minnesota",
    "Mississippi",
    "Missouri",
    "Montana",
    "Nebraska",
    "Nevada",
    "New Hampshire",
    "New Jersey",
    "New Mexico",
    "New York",
    "North Carolina",
    "North Dakota",
    "Ohio",
    "Oklahoma",
    "Oregon",
    "Pennsylvania",
    "Rhode Island",
    "South Carolina",
    "South Dakota",
    "Tennessee",
    "Texas",
    "Utah",
    "Vermont",
    "Virginia",
    "Washington",
    "West Virginia",
    "Wisconsin",
    "Wyoming",
]

monitaur = Monitaur(
    client_secret=os.getenv(
        "MONITAUR_CLIENT_SECRET", "5f9bff1b7146df9093af372dc87ec0e07775e791"
    ),
    base_url="https://monitaur-api-prod.herokuapp.com",
)


@cached(cache=TTLCache(maxsize=1024, ttl=7200))
def get_data():
    """
    Retrieves data for confirmed cases, transforms it for consumption with Pandas,
    and then caches it for 2 hours.
    """

    confirmed = pd.read_csv(BASE_URL, error_bad_lines=False)
    confirmed["Country/Region"].unique()

    regions = confirmed[confirmed["Country/Region"] == "US"]["Province/State"].to_dict()

    confirmed_raw = confirmed.copy()
    confirmed_raw = confirmed.drop(
        ["Lat", "Long", "Province/State", "Country/Region"], axis=1, inplace=False
    )

    return (
        confirmed_raw,
        confirmed.to_json(orient="records"),
        datetime.utcnow().isoformat() + "Z",
        regions,
    )


def get_states(regions):
    states_dict = {}
    for state in STATES:
        for key, value in regions.items():
            if state == value:
                states_dict[state] = key

    return states_dict


# @cached(cache=TTLCache(maxsize=1024, ttl=7200))
def calculate_totals(confirmed_cases, states):
    totals = []
    for state in STATES:
        for item in confirmed_cases:
            if state == item["Province/State"]:
                total_cases = 0
                new_cases = 0
                percent_change = 0
                cases = []
                for key, value in item.items():
                    if key not in ["Province/State", "Country/Region", "Lat", "Long"]:
                        prev_total = total_cases
                        total_cases = float(value)
                        new_cases = total_cases - prev_total
                        percent_change = (
                            (new_cases / prev_total) if prev_total else 0.00
                        ) * 100
                        cases.append(
                            {
                                "date": key,
                                "running_total": value,
                                "new_cases": new_cases,
                                "percent_change": percent_change,
                            }
                        )
                totals.append(
                    {
                        "state": item["Province/State"],
                        "total_cases": total_cases,
                        "new_cases": new_cases,
                        "percent_change": str(round(percent_change, 2)),
                        "increase_in_cases": cases,
                        "state_index": states[state],
                    }
                )

    for total in totals:
        total_prediction = predict(total["state_index"], total["state"])
        prediction = total_prediction - total["total_cases"]
        if prediction < 0:
            prediction = 0

        # three-day moving average
        dates = total["increase_in_cases"][-2:]
        sum_cases = 0
        for date in dates:
            sum_cases += date["new_cases"]
        average = sum_cases / 2

        final_prediction = (prediction * 0.1) + (average * 0.9)

        total["prediction"] = int(final_prediction)

    return totals


@cached(cache=TTLCache(maxsize=1024, ttl=7200))
def predict(state_index, state):
    confirmed_data_raw, confirmed_data_json, last_updated, regions = get_data()

    xhat, p, xhat_minus, p_minus, kalman_gain = kalman_filter(
        observations=confirmed_data_raw.iloc[state_index, :].values,
        initial_value=confirmed_data_raw.iloc[state_index, :].values[0],
        param_export=True,
    )
    xhat = kalman_filter_predict(
        xhat,
        p,
        xhat_minus,
        p_minus,
        kalman_gain,
        confirmed_data_raw.iloc[state_index, :].values,
        param_export=False,
    )

    forecast = arma_model(xhat)

    if current_app.config.get("ENVIRONMENT") == "prod":
        model_set_id = "406f37e7-4a53-43bc-aafc-e391b7741125"

        transaction_data = {
            "credentials": "foo",
            "model_set_id": model_set_id,
            "trained_model_hash": 1234,
            "production_file_hash": 1234,
            "prediction": forecast,
            "image": None,
            "features": {
                "xhat": xhat.tolist(),
                "state_index": state_index,
                "state": state,
            },
        }
        monitaur.record_transaction(**transaction_data)

    return forecast
