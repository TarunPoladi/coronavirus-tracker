# project/api/kalman.py

import numpy as np
from statsmodels.tsa.arima_model import ARIMA

variance = 0.0005


def kalman_filter(observations, initial_value, truth_values=None, param_export=False):
    """
    Description:
        Function to create a Kalman Filter for smoothing currency timestamps in order to search for the
        intrinsic value.

    Parameters:
        observations: Array of observations -- i.e., predicted secondary market prices.
        initial_value: Initial Starting value of filter
        truth_values: Array of truth values, i.e. GPS location or secondary market prices. Or can be left
                      blank if none exist
        plot: If True, plot the observations, truth values and kalman filter.
        param_export: If True, parameters_hat, p, xhat_minus, p_minus, kalman_gain are returned to use in training.

    Example:
        xhat, p, xhat_minus, p_minus, kalman_gain = kalman_filter(
            observations=ODE_Daily.close.values[0:-1],
            initial_value=ODE_Daily.close.values[-1],
            param_export=True,
        )
    """
    # initial parameters
    number_of_iterations = len(observations)
    length_of_the_array = (number_of_iterations,)
    z = observations  # observations (normal about x, sigma=0.1)

    Q = 1e-5  # process variance

    # allocate space for arrays
    xhat = np.zeros(length_of_the_array)  # a posteri estimate of x
    p = np.zeros(length_of_the_array)  # a posteri error estimate
    xhat_minus = np.zeros(length_of_the_array)  # a priori estimate of x
    p_minus = np.zeros(length_of_the_array)  # a priori error estimate
    kalman_gain = np.zeros(length_of_the_array)  # gain or blending factor

    R = variance ** 2  # estimate of measurement variance, change to see effect

    # initial guesses
    xhat[0] = initial_value
    p[0] = 1.0

    for num in range(1, number_of_iterations):
        # time update
        xhat_minus[num] = xhat[num - 1]
        p_minus[num] = p[num - 1] + Q

        # measurement update
        kalman_gain[num] = p_minus[num] / (p_minus[num] + R)
        xhat[num] = xhat_minus[num] + kalman_gain[num] * (z[num] - xhat_minus[num])
        p[num] = (1 - kalman_gain[num]) * p_minus[num]

    if param_export is True:
        return xhat, p, xhat_minus, p_minus, kalman_gain
    else:
        return xhat


def kalman_filter_predict(
    xhat,
    p,
    xhat_minus,
    p_minus,
    kalman_gain,
    observations,
    truth_values=None,
    param_export=False,
):
    """
    Description:
        Function to predict a pre-trained Kalman Filter 1 step forward.

    Parameters:
        xhat: Trained Kalman filter values - array
        p: Trained Kalman variance - array
        xhat_minus: Trained Kalman xhat delta - array
        p_minus: Trained Kalman variance delta - array
        kalman_gain: Kalman gain - array
        observations: Array of observations -- i.e., predicted secondary market prices.
        truth_values: Array of truth values -- i.e., GPS location or secondary market prices. Or can be left blank.
        param_export: If True, parameters xhat,P,xhat_minus,p_minus,K are returned to use in next predictd step.

    Example:
        xhat, p, xhat_minus, p_minus, kalman_gain = kalman_filter_predict(
            xhat,
            p,
            xhat_minus,
            p_minus,
            kalman_gain,
            confirmed_data_raw.iloc[state_index, :].values,
            param_export=True,
        )
    """
    # initial parameters
    z = observations  # observations (normal about x, sigma=0.1)
    Q = 1e-5  # process variance
    R = variance ** 2  # estimate of measurement variance, change to see effect

    # time update
    xhat_minus = np.append(xhat_minus, xhat[-1])
    p_minus = np.append(p_minus, p[-1] + Q)

    # measurement update
    kalman_gain = np.append(kalman_gain, p_minus[-1] / (p_minus[-1] + R))
    xhat = np.append(xhat, xhat_minus[-1] + kalman_gain[-1] * (z[-1] - xhat_minus[-1]))
    p = np.append(p, (1 - kalman_gain[-1]) * p_minus[-1])

    if param_export is True:
        return xhat, p, xhat_minus, p_minus, kalman_gain
    else:
        return xhat


def arma_model(xhat):
    # Autoregressive–moving-average model (ARMA)
    # orders = [
    #     (3, 2, 1),
    #     (3, 2, 0),
    #     (3, 1, 1),
    #     (3, 1, 0),
    #     (1, 0, 0)
    # ]
    try:
        model = ARIMA(xhat, order=(3, 2, 1),)
        model_fit = model.fit(disp=0)
    # except Exception:
    #     try:
    #         model = ARIMA(xhat, order=(3, 2, 0),)
    #         model_fit = model.fit(disp=0)
    #     except Exception:
    #         try:
    #             model = ARIMA(xhat, order=(3, 1, 1),)
    #             model_fit = model.fit(disp=0)
    #         except Exception:
    #             try:
    #                 model = ARIMA(xhat, order=(3, 1, 0),)
    #                 model_fit = model.fit(disp=0)
    except Exception:
        model = ARIMA(xhat, order=(1, 1, 0),)
        model_fit = model.fit(disp=0)

    forecast, stderr, conf = model_fit.forecast(steps=1, alpha=0.05)
    return forecast[0]
