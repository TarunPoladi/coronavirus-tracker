// custom javascript

const data = {};

let myChart;

$(document).ready(function() {
  console.log('Sanity Check!');

  $('.loader-wrapper').addClass('is-active');
  $('#last-updated').hide();

  // get all data
  getData();
});

function changeChart() {
  const city = document.getElementById('select-state').value;
  const type = document.getElementById('select-type').value;
  const ctx = document.getElementById('my-chart').getContext('2d');
  const temp = getConfig(city, type);

  if (myChart) { myChart.destroy(); }

  myChart = new Chart(ctx, temp);
}

function getConfig(state, type){
  if (type === 'null'){
    type = 'line';
  }

  if (state === 'null') {
    state = 'New York';
  }

  return {
    // The type of chart we want to create
    type: type,

    // The data for our dataset
    data: {
      labels: data[state][0],
      datasets: [{
        label: `Increase in Cases for ${state}`,
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgb(255, 99, 132)',
        data: data[state][1]
      }]
    },

    // Configuration options go here
    options: {
      scales: {
        xAxes: [
          {
            ticks:{
              display: true,
              autoSkip: true,
              maxTicksLimit: 10
            }
          }
        ],
      }
    }
  }
}

function getData() {
  $.ajax({
    url: '/api/coronavirus/summary',
  })
  .done((res) => {
    // add table data
    const summaries = res.summary;
    let table = $('.table').DataTable({
      columns: [
        {data: 'state' },
        {data: 'total_cases'},
        {data: 'new_cases'},
        {data: 'percent_change', 'type': 'num'},
        {data: 'prediction', 'type': 'num'},
      ],
      order: [[ 1, 'desc' ]],
      paging: false,
      fixedHeader: true,
    });
    table.rows.add(summaries).draw();

    const cities = document.getElementById('select-state');
    const startDate = new Date('3/1/2020');  // only show dates >= 3/2/20

    for (const item of summaries){
        const el = document.createElement('option');
        el.textContent = item['state'];
        el.value = item['state'];
        cities.appendChild(el);

        const date = [];
        const newCases = [];

        for (const i of item['increase_in_cases']) {
            if (new Date(i.date) >= startDate) {
              date.push(i.date);
              newCases.push(i.new_cases);
            }
        }
        data[item.state] = [date, newCases];
    }

    const ctx = document.getElementById('my-chart').getContext('2d');
    myChart = new Chart(ctx, getConfig('New York', 'line'));


    // sum totals
    const column_total_cases = table.column(1);
    const total_cases_sum = column_total_cases.data().reduce((a, b) => a + b);
    $(column_total_cases.footer()).html(total_cases_sum);
    const column_daily_sum = table.column(2);
    const total_daily_sum = column_daily_sum.data().reduce((a, b) => a + b);
    $(column_daily_sum.footer()).html(total_daily_sum);
    const column_prediction_sum = table.column(4);
    const total_prediction_sum = column_prediction_sum.data().reduce((a, b) => Number(a) + Number(b));
    $(column_prediction_sum.footer()).html(total_prediction_sum);

    // add last updated
    $('#last-updated-date').html(new Date(res.last_updated));
    $('.dataTables_filter input[type="search"]').addClass('input is-small').css('width', 'auto');
  })
  .fail((err) => {
    console.log(err);
  })
  .always(() => {
    $('#last-updated').show();
    $('.loader-wrapper').removeClass('is-active');
  })
}
